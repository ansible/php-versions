# PHP versions

Allows different PHP versions to be installed when using the `php` role.
Derived from Jeff Geerling's role of the same name.


## Role variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    php_version: '7.3'

The PHP version to be installed. Any [currently-supported PHP major version](http://php.net/supported-versions.php) is a valid option (e.g. `7.2`, `7.3`, `7.4` etc.).

    php_versions_install_recommends: false

(For Debian OSes only) Whether to install recommended packages. This is set to `no` by default because setting it to `yes` often leads to multiple PHP versions being installed (thus making a bit of a mess) when using repos like Ondrej's PHP PPA for Ubuntu.


## Reverse dependencies: roles that depend on `php` or `php-versions`

```
../playbooks/luxor/roles/monica/meta/main.yml:5:  - { role: php, become: true, tags: php, when: "'workstation' not in group_names" }
../roles/public/R/meta/main.yml:14:  - role: php
../roles/public/apache/meta/main.yml:13:  - role: php
../roles/loc/drush/meta/main.yml:5:  - { role: php, become: true, tags: php, when: "'workstation' not in group_names" }
../roles/loc/wordpress/meta/main.yml:7:  - { role: php, become: true, tags: php, when: "'workstation' not in group_names" }
../roles/dev/composer/meta/main.yml:10:  - role: php
../roles/dev/dashboard-heimdall/meta/main.yml:5:  - { role: php, become: true }
../roles/dev/redis/meta/main.yml:12:  - role: php
../roles/dev/matomo/meta/main.yml:7:  - { role: php, become: true, tags: php, when: "'workstation' not in group_names" }
```


## License

MIT / BSD
